import { Component, OnInit, AfterViewInit, ViewChild, TemplateRef } from '@angular/core';
import { UserService } from './../../services/user.service';
import { MatSort, Sort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { GetAllUsers } from './../../models/Datatables/datatablesModels';
import { DatatableArrays } from 'src/app/models/Datatables/datatableArrays';
import {MatDialog} from '@angular/material/dialog';


@Component({
  selector: 'app-admin-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],


})
export class UsersComponent implements OnInit,  AfterViewInit {
  orders: any;
  ordersResults = 0;
  userStatus = 'Delete';
  refill: false;
  obj:any;
  datatableArrays: DatatableArrays = new DatatableArrays();


  constructor(private userService: UserService, private _liveAnnouncer: LiveAnnouncer, public dialog: MatDialog) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('secondDialog') secondDialog: TemplateRef<any>;

  ngOnInit(){
    this.getUsers();
  }
  ngAfterViewInit(){
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public dataSource = new MatTableDataSource<GetAllUsers>();

  getUsers(){
    this.userService.getUsers().subscribe(data => {
      console.log('data of users', data);
      this.dataSource.data = data.users as GetAllUsers[];
      console.log('this.dataSource.data', this.dataSource.data)
    });

    for (this.obj of this.dataSource.data) {
      if (this.obj.status === '0') {this.ordersResults++;}
    }

    console.log(this.ordersResults);
    
  }

  announceSortChange(sortState: Sort) {

    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  displayedColumns: string[] = this.datatableArrays.getUsersArr();

  openDialogWithRef(ref: TemplateRef<any>, deletes) {
    this.userStatus = deletes;
    this.dialog.open(ref);
    console.log(this.userStatus)
  }


  
  deleteUser(element){
    let userId ={userId: element._id};
    this.userService.deleteUser(userId).subscribe(data => {
      console.log('data', data)
    })
  }

  blockUser(element){
    let userId ={userId: element._id};
    this.userService.blockUser(userId).subscribe(data => {
      console.log('data', data)
    })
  }


}