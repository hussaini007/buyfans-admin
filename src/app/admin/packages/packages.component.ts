import { FollowizService } from './../../services/followiz.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Package, CreatePlatform } from '../../models/followiz';
import { PackageService } from '../../services/package.service';

import { MatSort, Sort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { GetAllPackages } from './../../models/Datatables/datatablesModels';
import { DatatableArrays } from 'src/app/models/Datatables/datatableArrays';

@Component({
  selector: 'app-admin-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.css'],
  providers: [NgbModalConfig, NgbModal]
})

export class AdminPackagesComponent implements OnInit {
  package: Package = new Package();
  createPlatform: CreatePlatform = new CreatePlatform();
  categories: any;
  packages: any;
  providers: any;
  platforms: any;
  services: any;
  orders: any;
  ordersResults = 0;
  refill: false;
  obj:any;
  dataSource = new MatTableDataSource<GetAllPackages>();
  datatableArrays: DatatableArrays = new DatatableArrays();

  constructor(private followizService: FollowizService, private packageService: PackageService, config: NgbModalConfig, private modalService: NgbModal, private _liveAnnouncer: LiveAnnouncer) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
    this.getCatogories();
    this.getPackages();
    this.getProvider();
    this.getPlatform();
  }


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  ngAfterViewInit(){
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }




  getPackages() {
    this.packageService.getPackages().subscribe(data => {
      console.log('data in get packages', data)
      this.dataSource.data = data.package as GetAllPackages[];
      console.log('this.dataSource.data', this.dataSource.data)
    })
  
    for (this.obj of this.dataSource.data) {
      if (this.obj.status === '0') {this.ordersResults++;}
    }
  
    console.log(this.ordersResults);
  }
  
  announceSortChange(sortState: Sort) {
  
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
  
  displayedColumns: string[] = this.datatableArrays.getPackagesArr();

  savePackage() {
    console.log('this.package', this.package);
    this.packageService.savePackageService(this.package).subscribe(data => {
      console.log('data', data)
    })
  }


  refillValue(event) {
    this.package.refill = event.target.value;
    console.log(this.package.refill);
  }

  packageStatusValue(event) {
    this.package.status_of_package = event.target.value;
    console.log(this.package.status_of_package);
  }

  packageFeaturedValue(event) {
    this.package.featured = event.target.value;
    console.log(this.package.featured);
  }



  getCatogories() {
    this.followizService.getCategories().subscribe(data => {
      this.categories = data.categories;
      console.log(data);
    });
  }

  ProviderValue(event) {
    console.log('provider', event.target.value);
    this.package.provider= event.target.value;
    this.followizService.fetchServices(this.package.provider).subscribe(data => {
      this.services = data.data;
      console.log('this.services ',this.services );
    })
  }

  serviceValue(event){
    this.package.service_name = this.services[event.target.value].name;
    this.package.service = this.services[event.target.value].service;
    this.package.service_rate = this.services[event.target.value].rate;
  }

  getProvider() {
    {
      this.followizService.getProviders().subscribe(data => {
        console.log('data', data);
        this.providers = data.provider;
      })
    }
  }

  getPlatform() {
    {
      this.packageService.getPlatform().subscribe(data => {
        console.log('data', data);
        this.platforms = data.platform;
      })
    }
  }

  categoryValue(event) {
    this.package.category = this.categories[event.target.value].name;
    console.log('this.package.category ',this.package.category )
   this.package.categoryId = this.categories[event.target.value]._id;
  }

  platformValue(event) {
    this.package.platform = this.platforms[event.target.value].name;
   this.package.platformId = this.platforms[event.target.value]._id;
  }
  show(category) {
    this.modalService.open(category);
  }

  makePlatform() {
    this.packageService.createPlatform(this.createPlatform).subscribe(data => {
      console.log('data', data);
    })
  }

  open(platform) {
    this.modalService.open(platform);
  }
}
