import { ProfilePicService } from '../../services/profile-pic.service';
import { UserService } from '../../services/user.service';
import { ToastService } from '../../services/toast.service';
import { BasicProfile, UpdateProfile, SecurityDetail } from '../../models/profileSetting';
import { AuthService } from '../../services/auth.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
// import { ProfilePicService}
// import { BasicProfile}

@Component({
  selector: 'app-admin-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class AdminSettingsComponent implements OnInit {

  constructor(public element: ElementRef,private toast:ToastService,
    private authService:AuthService,
    private userService:UserService
    ,private profilePicture:ProfilePicService) { }
    imageChanged:boolean=false;
    files: any;
    imgName:any;
    imgURL:any;
    imageRead:any=0;
    fileToUpload:FormData=new FormData();
    basicProfile:BasicProfile=new BasicProfile();
    updateProfile:UpdateProfile=new UpdateProfile();
    security:SecurityDetail=new SecurityDetail();
    user:any;
    userDetials:any;
    imgExists:boolean=false;
    loader:boolean=false;

    ngOnInit(): void {
      this.user=JSON.parse(localStorage.getItem('user'));
      this.basicProfile.fullName = this.user.fullName;
      this.basicProfile.phone = this.user.phone;
      this.basicProfile.email = this.user.email;
      // this.basicProfile.profile_pic_url= this.user.profile_pic_url;
      // this.basicProfile.profile_pic_url = this.userDetials.profile_pic_url;
      this.userDetials=JSON.parse(localStorage.getItem('user_image'));
      // console.log('user details',this.userDetials)
      if(this.userDetials == null || !this.userDetials){
        this.imgExists = false;
      }
      else{
        this.imgExists = true;
      }
    }
  
  
  
  
  
    async fileChange(event) {
  
      this.imageChanged=true;
      this.files = this.element.nativeElement.querySelector('#imageUpload').files;
  
      const file = this.files[0];
      this.fileToUpload=new FormData();
      //
      this.imgName = file.name;
      this.fileToUpload.append('avatar', file,this.imgName);
      
      try {
        this.imgURL = await this.readFile(file);
        this.imageRead = 1;
        
      } catch (e) {
        
      }
    }
  
    readFile = (file) => {
      const fileReader = new FileReader();
      return new Promise((resolve, reject) => {
        fileReader.onerror = () => {
          fileReader.abort();
          
          reject('aborted');
        };
        fileReader.onload = () => {
          resolve(fileReader.result);
        };
        fileReader.readAsDataURL(file);
      });
    }
  
  
  
  
  
  
  
    saveProfileImage(){
      
      this.loader=true;
      // console.log(this.fileToUpload.get('avatar'));
      this.userService.saveProfileImage(this.fileToUpload).subscribe((data)=>{
        
        this.loader=false;
        // this.user=data.user.profile_pic_url.toString();
        this.userDetials=data.user.profile_pic_url;
         //localStorage.removeItem('user');
        // localStorage.setItem('user', this.user);
        localStorage.setItem('user_image', JSON.stringify(this.userDetials));
        this.toast.successToaster('Profile Image Updated Successfully','Profile Image');
        this.profilePicture.loadPicture(data.user.profile_pic_url);
        // this.profilePicture.loadPicture(this.user);
        this.imageChanged=true;
      },error=>{
        
        this.loader=false;
        this.toast.errorToaster('Profile Image Was Not Uploaded','Profile Image');
        this.imageChanged=true;
      })
    }
    saveProfileChanges(){
    
      this.loader=true;
      this.userService.saveProfileChange(this.basicProfile).subscribe((data)=>{
        console.log('data', data);
        this.loader=false;
        this.user.fullName=this.basicProfile.fullName;
        this.user.email=this.basicProfile.email;
        this.user.phone=this.basicProfile.phone;
        // localStorage.removeItem('user');
        console.log('user', this.user);
        localStorage.setItem('user',JSON.stringify(this.user));
        this.toast.successToaster('Profile Updated Successfully','Profile');
        this.onlyShowForm('btn-edit','editable-form');
      },
      error=>{
        
        this.loader=false;
        this.toast.errorToaster('Phone/Email Already Exists','Profile');
      });
    }
    savePasswordChange(){
      
      this.loader=true;
      this.userService.saveSecurityChange(this.security).subscribe((data)=>{
        console.log('data', data);
        this.loader=false;
        this.toast.successToaster('Password Updated Successfully','Security');
        this.onlyShowForm('edit-btn1','editable-form1',1);
      },
      error=>{
       
        this.loader=false;
        this.toast.errorToaster(`${error.error}`,'Security');
      })
    }
  
  

  
    editForm(edit,formId,int?){
      let editBtn=document.getElementById(edit);
      let form=document.getElementById(formId);
      let buttons_group=document.getElementById("buttons_group");
      let buttons_group2=document.getElementById("buttons_group2");


      (<HTMLInputElement>form.children[0]).disabled=false;
      (<HTMLInputElement>form.children[1]).disabled=false;
      (<HTMLInputElement>form.children[2]).disabled=false;
      (<HTMLDivElement>form.lastChild).style.display="block";
      editBtn.style.display="none";

      buttons_group.querySelectorAll('button').forEach(button2=>{
        button2.style.display='inline-block';
      });
      buttons_group2.querySelectorAll('button').forEach(button=>{
        button.style.display='inline-block';
      });

   
      if(int==1){
        (<HTMLInputElement>form.children[1]).style.display="block";
        (<HTMLInputElement>form.children[2]).style.display="block";
      }
    }
    onlyShowForm(edit,formId,int?){
      let editBtn=document.getElementById(edit);
      let form=document.getElementById(formId);
      (<HTMLInputElement>form.children[0]).disabled=true;
      (<HTMLInputElement>form.children[1]).disabled=true;
      (<HTMLInputElement>form.children[2]).disabled=true;
      (<HTMLDivElement>form.lastChild).style.display="none";
      editBtn.style.display="block";
      if(int==1){
  
        (<HTMLInputElement>form.children[1]).style.display="none";
        (<HTMLInputElement>form.children[2]).style.display="none";
      }
    }
  
  
    userName(){
      // console.log('user', this.user)
      // return this.user.fullName;
    }
}
