import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';
import { MatSort, Sort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { GetAllOrders } from './../../models/Datatables/datatablesModels';
import { DatatableArrays } from 'src/app/models/Datatables/datatableArrays';

@Component({
  selector: 'app-admin-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements  OnInit,  AfterViewInit  {

  orders: any;
  ordersResults = 0;
  refill: false;
  obj:any;
  datatableArrays: DatatableArrays = new DatatableArrays();

  constructor(private orderService: OrderService, private _liveAnnouncer: LiveAnnouncer) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(){
    this.getUsers();
  }
  ngAfterViewInit(){
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public dataSource = new MatTableDataSource<GetAllOrders>();

  getUsers(){
    this.orderService.getOrders().subscribe(data => {
      console.log('data', data);
            this.dataSource.data = data.orders as GetAllOrders[];
      console.log('this.dataSource.data', this.dataSource.data)
    })

    for (this.obj of this.dataSource.data) {
      if (this.obj.status === '0') {this.ordersResults++;}
    }

    console.log(this.ordersResults);
    
  }

  announceSortChange(sortState: Sort) {

    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  displayedColumns: string[] = this.datatableArrays.getOrdersArr();
}
