
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Service, BuyService, BuyProvider, SerivceDataTable, CreateCategory } from '../../models/followiz';
import { FollowizService } from '../../services/followiz.service';

import { MatSort, Sort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { GetAllServices } from './../../models/Datatables/datatablesModels';
import { DatatableArrays } from 'src/app/models/Datatables/datatableArrays';

@Component({
  selector: 'app-admin-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
  providers: [NgbModalConfig, NgbModal]
})

export class ServicesComponent implements OnInit, AfterViewInit {
  services: any;
  allProviders: any;
  selected: any;
  value: any;
  fixed: any;
  percentage: any;
  total: any;
  allServices: any;
  categories: any;
  selectCategory: any;
  cateogryId: any;
  cateogryName: any;
  orders: any;
  ordersResults = 0;
  refill: false;
  obj:any;
  datatableArrays: DatatableArrays = new DatatableArrays();

  single_service: Service = new Service();
  buy_service: BuyService = new BuyService();
  buy_provider: BuyProvider = new BuyProvider();
  createCategory: CreateCategory = new CreateCategory();
  dataSource = new MatTableDataSource<GetAllServices>();

  constructor(private followizService: FollowizService, config: NgbModalConfig, private modalService: NgbModal, private _liveAnnouncer: LiveAnnouncer) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
    this.getProvider();
    this.getCategories();
    this.getAllServices();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  ngAfterViewInit(){
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }



getAllServices(){
  this.followizService.getServices().subscribe(data => {
    console.log('datasssss', data)
    this.dataSource.data = data.service as GetAllServices[];
    console.log('this.dataSource.data', this.dataSource.data)
  })

  for (this.obj of this.dataSource.data) {
    if (this.obj.status === '0') {this.ordersResults++;}
  }

  console.log(this.ordersResults);
}

announceSortChange(sortState: Sort) {

  if (sortState.direction) {
    this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
  } else {
    this._liveAnnouncer.announce('Sorting cleared');
  }
}

displayedColumns: string[] = this.datatableArrays.getServicesArr();


  getServices(event): any {
    const value = event.target.value;
    this.selected = value;
    this.buy_provider.name = value;
    this.value = value;
    this.followizService.fetchServices(value).subscribe(data => {
      console.log('data from the backedn', data);
      this.services = data.data;

    })
  }
  serviceName(event): any {
    this.single_service = this.services[event.target.value];
    this.buy_service.name = this.single_service.name;
    this.buy_service.refill = this.single_service.refill;
    this.buy_service.dripfeed = this.single_service.dripfeed;
    this.buy_service.provider = this.value;
    console.log('buy service in the component:', this.buy_service);
    // console.log('single service', this.single_service);
  }

  calculatePrice() {
    let number_parsed: number = parseFloat(this.single_service.rate);
    let fixed_number = this.fixed + number_parsed;
    let percentage_number = (this.percentage * number_parsed) / 100;
    this.total = fixed_number + percentage_number;
  }

  categoryValue(event) {
    const value = event.target.value;
    console.log('value', value);
    this.cateogryId = this.categories[event.target.value]._id;
    this.selectCategory = this.categories[event.target.value].name;
    this.buy_service.category = this.selectCategory;

  }
  
  



  saveService() {
    this.buy_service.rate = this.total;
    this.buy_service.Id = this.single_service.service;
    this.buy_service.categoryId = this.cateogryId;
    this.followizService.saveServices(this.buy_service).subscribe(data => {
      console.log('data', data);
    })
  }
  saveProvider() {
    this.followizService.saveProvider(this.buy_provider).subscribe(data => {
      console.log('data', data);
    })
  }

  getProvider() {
    {
      this.followizService.getProviders().subscribe(data => {
        console.log('data', data);
        this.allProviders = data.provider;
      })
    }
  }

  makeCategory() {
    this.followizService.createCategories(this.createCategory).subscribe(data => {
      console.log('data', data);
    })
  }

getCategories() {
  this.followizService.getCategories().subscribe(data => {
    console.log( 'data', data);
    this.categories = data.categories;
    console.log('categories', this.categories);
  })
}

  open(content) {
    this.modalService.open(content);
  }
  modal(provider) {
    this.modalService.open(provider);
  }
  show(category) {
    this.modalService.open(category);
  }

}

