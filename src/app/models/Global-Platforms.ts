import { ArrayType } from '@angular/compiler';

export class GlobalConstants {

  public static main_categories = ["facebook","youtube","instagram","twitter","linkedin"];
  //Sub Categories
  public static facebook = ["post-likes", "post-views", "page-likes", "page-views", "reactions"];
  public static youtube = ["video-likes", "video-views", "video_dislikes", "subscribers"];
  public static instagram = ["post-likes","post-views","video-likes","followers"];
  public static twitter = ["retweets","views","reactions"];
  public static linkedin = ["likes", "connections", "post reactions", "page-likes"];
  public static sub_categories = [];
  // GlobalConstants.sub_categories.push(art);
  public static sub_categories_function (index){
    // var arr = [];
    this.sub_categories.push(this.facebook);
    this.sub_categories.push(this.youtube);
    this.sub_categories.push(this.instagram);
    this.sub_categories.push(this.twitter);
    this.sub_categories.push(this.linkedin);
    return this.sub_categories[index];
    // return arr;
  }

}
