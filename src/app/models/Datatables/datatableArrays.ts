export class DatatableArrays {
    getUsersArr(){
        return ['fullName', 'phone', 'email', 'cart_items', 'user_status', 'role', 'options'];
    }

    getOrdersArr(){
        return ['name', 'category', 'status', 'quantity', 'rate', 'link', 'refill','email', 'fullName', 'provider', 'provider_rate', 'provider_order_id'];
    }

    getServicesArr(){
        return ['name', 'category', 'rate', 'min', 'max', 'speed_per_day', 'provider'];
    }

    getPackagesArr(){
        return ['name', 'category', 'platform', 'quantity', 'refill', 'provider', 'featured', 'promotion', 'service', 'service_rate', 'option1', 'option2', 'option3', 'option4', 'option5', 'dropdown'];
    }
}

// name
// category
// platform
// rate
// quantity
// refill
// provider

// featured
// promotion
// service
// service_rate
// status_of_package
// option1
// option2
// option3
// option4
// option5