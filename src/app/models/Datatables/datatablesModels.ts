export interface GetAllUsers {
    start_count: number;
    fullName: string;
    phone: number;
    email: string;
    cart_items: number;
    user_status: string;
    role: string;
}

export interface GetAllOrders {
    name: string
    category: string;
    status: string;
    quantity: number;
    rate: number;
    link: string;
    refill: string;
    email: string;
    fullName: string;
    provider: string;
    provider_rate: number;
    provider_order_id: number;
}

export interface GetAllServices {
    name: string;
    category: string;
    rate: number;
    Id: number;
    min: number;
    max: number;
    speed_per_day: number;
    provider: string;

}


export interface GetAllPackages {
    name: string;
    category: string;
    platform: string;
    rate: number;
    quantity: number;
    refill: string;
    provider: string;
    
    featured: string;
    promotion:any;
    service: string;
    service_rate: number;
    status_of_package: string;

    option1: string;
    option2: string;
    option3: string;
    option4: string;
    option5: string;
    


}


// name
// category
// platform
// rate
// quantity
// refill
// provider

// featured
// promotion
// service
// service_rate
// status_of_package
// option1
// option2
// option3
// option4
// option5
