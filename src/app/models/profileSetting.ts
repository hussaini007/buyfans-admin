export class BasicProfile{
    fullName:string;
    email:string;
    phone:string;
}

export class UpdateProfile{
    fullName:string;
    email:string;
    phone:string;
}

export class SecurityDetail{
    password:string;
    newPassword:string;
    confirmPassword:string;
}

