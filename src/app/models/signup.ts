export class Signup {
    fullName:string;
    email:string;
    phone: Number;
    password:string;
    confirmPassword:string;
}

export class User {
    cart_items: number;
    email: string;
    fullName: string;
    password: string;
    phone: number;
    profile_pic_url: string;
    role: string;
    stripe_id: string;
    user_status: string;
    _id: string;
    updatedAt: string;
    createdAt: string;
}