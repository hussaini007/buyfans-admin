export class Service {
    category: string;
    dripfeed: boolean;
    max: number;
    min: number;
    name: string;
    rate: any;
    refill: boolean;
    service: number;
    type: string;
    provider: string;
}


export class BuyService {
    Id: number;
    categoryId: any;
    name: string;
    category: string;
    mode: boolean;
    provider: string;
    service: string;
    rate: number;
    fixed: number;
    percentage: number;
    min: number;
    max: number;
    dripfeed: boolean;
    refill: boolean;
    speed: any;
}

export class BuyProvider {
    name: string;
    link: string;
    api: any;
    action: any;
}

export class SerivceDataTable {
    category: string;
    name: string;
    refill: boolean;
    provider: string;
    rate: number;
    min: number;
    max: number;
    speed_per_day: number
}

export class CreateCategory {
    name: string;
}

export class CreatePlatform {
    name: string;
}


export class Package {
    categoryId: any;
    category: any;
    platformId: any;
    platform: any;
    name: string;
    quantity: number;
    rate: number;
    option1: string;
    option2: string;
    option3: string;
    option4: string;
    option5: string;
    refill: boolean;
    promotion: any;
    status_of_package: boolean;
    featured: any;
    provider: any;
    service: any;
    service_name: any;
    service_rate: any;
}

