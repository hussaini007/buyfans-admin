import { TasksComponent } from './admin/tasks/tasks.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';  
import {FormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatDialogModule} from '@angular/material/dialog';


import { AdminPackagesComponent } from './admin/packages/packages.component';
import { ServicesComponent } from './admin/service/services.component';
import { AdminSidebarComponent } from './admin/sidebar/sidebar.component';
import { AdminSettingsComponent } from './admin/settings/settings.component';
import { AdminDashboardComponent } from './admin/dashboard/dashboard.component';
import { TicketsComponent } from './admin/tickets/tickets.component';
import { ReportsComponent } from './admin/reports/reports.component';
import { AppearanceComponent } from './admin/appearance/appearance.component';
import { PaymentsComponent } from './admin/payments/payments.component';
import { CancelComponent } from './admin/cancel/cancel.component';
import { DripFeedComponent } from './admin/dripfeed/dripfeed.component';
import { SubscriptionsComponent } from './admin/subscriptions/subscriptions.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { UsersComponent } from './admin/users/users.component';
import { TicketsMessageComponent } from './admin/tickets/tickets-message/tickets-message.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    ServicesComponent,
    AdminSidebarComponent,
    AdminDashboardComponent,
    AdminSettingsComponent,
    UsersComponent,
    OrdersComponent,
    SubscriptionsComponent,
    DripFeedComponent,
    CancelComponent,
    AdminPackagesComponent,
    PaymentsComponent,
    AppearanceComponent,
    ReportsComponent,
    TicketsComponent,
    TasksComponent,
    TicketsMessageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
