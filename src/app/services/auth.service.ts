import { UserService } from './user.service';
import { User } from './../models/user';
import { Router } from '@angular/router';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable , Subject} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

	private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
  	public userRoleSubject: BehaviorSubject<any>;
  	public userRole: Observable<any>;

  	private subject = new Subject<any>();

    private headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    private getHeaders() {
      const token = localStorage.getItem('token');
      const headers = this.headers.append('Authorization', 'Bearer '+ token);
      return headers;
    }

  constructor(private API :ApiService,private router: Router) {
  	this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.userRoleSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('role')));
    this.userRole = this.userRoleSubject.asObservable();
   }



  	public get currentUserValue(): User {
    	return this.currentUserSubject.value;
  	}
  	userRoleSubjectNext(value): any {
    	return this.userRoleSubject.next(value);
  	}

 //  	public getUser(): any {
 //     const user = new Observable(observer => {
 //            setTimeout(() => {
 //                observer.next(this.currentUserSubject.value);
 //            }, 1000);
 //     });

 //     return user;
 // }

	// upload(file) {

	// 	const headers = new HttpHeaders({
	// 	'mimeType': 'multipart/form-data'
	// 	});
	// 	headers.append('Accept', 'application/json');
	// 	return this.API.post('register/user', file, headers);
	// }

	login(user) {

		return this.API.post('user/auth/login', user).pipe(map(data => {
      console.log('data.user =', data.user);
            // login successful if there's a jwt token in the response
            if (data.user && data.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('token', data.token);
                localStorage.setItem('user', JSON.stringify(data.user));
                localStorage.setItem('user_image', JSON.stringify(data.user.profile_pic_url));
                localStorage.setItem('role', JSON.stringify(data.user.role));

                this.currentUserSubject.next(data.user);
                // this.currentUserSubject.complete();
                console.log(data.user);
              }
              this.sendMessage(data.user.role);
              // location.reload();
            return data.user;
        }));

  }
  
  getHomeData(){
    return this.API.get('');
  }
  resetPasswordRequest(data){
    return this.API.post('resetPasswordRequest', data);
  }
  resetPassword(data){
    return this.API.post('resetPassword', data);
  }
	  sendMessage(role) {
        this.subject.next(role);
    }
    // clearMessages() {
    //     this.subject.next();
    // }

    getRole(): Observable<any> {
        return this.subject.asObservable();
    }




	role(){
		var user=JSON.parse(localStorage.getItem('user'));
		return user.role;
	}

	logout() {
        // remove user from local storage to log user out
    	// // window.localStorage.clear();
      // console.log('in logout');

      var token = localStorage.getItem('token');
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ token
      });
      // headers.append('Authorization', 'Bearer '+ token);
      this.currentUserSubject.next(null);
      localStorage.clear();
      this.router.navigate(['login']);

      this.API.get('user/auth/logout',headers).subscribe(data => {
        // login successful if there's a jwt token in the response


    });

      // headers.append('Authorization', 'Bearer '+ token);

    }

	loggedIn(){
		if(localStorage.getItem('token'))
		{
			return true;
		}
		return false;
	}

	register(user) {
    console.log(user);
		return this.API.post('user/auth/signup', user).pipe(map(data => {
      // login successful if there's a jwt token in the response
      if (data.user && data.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes



          console.log(data);

          localStorage.setItem('token', data.token);
          localStorage.setItem('user', JSON.stringify(data.user));
          localStorage.setItem('role', JSON.stringify(data.user.role));

          this.currentUserSubject.next(data.user);
      }
      this.sendMessage(data.user.role);
      return data.user;
    }));
  }

  uploadFile(file) {

		const headers = new HttpHeaders({
    'mimeType': 'multipart/form-data',
    });
    headers.append('Accept', 'application/json');

		return this.API.post('uploadFile', file, headers);
  }
  removeUpload(file) {

		const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
		return this.API.post('removeUpload', file, headers);
  }

}
