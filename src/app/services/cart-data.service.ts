import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartDataService {

  private reRunCartDataCalculation=new BehaviorSubject<boolean>(false);
  reRunCartDataCalculationOnSignIn=this.reRunCartDataCalculation.asObservable();

  private setCartData=new BehaviorSubject<number>(0);
  setCartDataOnCheckout=this.setCartData.asObservable();

  private minusCartData=new BehaviorSubject<number>(0);
  minusCartDataOnCheckout=this.minusCartData.asObservable();


  private plusCartData=new BehaviorSubject<number>(0);
  plusCartDataOnCheckout=this.plusCartData.asObservable();
  constructor(){}

  setCartDataNumber(number){
      this.setCartData.next(number);
  }

  minus(number){
      this.minusCartData.next(number);
  }

  plus(number){
      this.plusCartData.next(number);
  }

  reRun(flag){
      this.reRunCartDataCalculation.next(flag);
  }
}

