import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private API: ApiService) { }

  getServiceList(){
    console.log('hit');
  }

  getOrders() {  
    return this.API.get('user/order/getAllOrders');
  }
}
