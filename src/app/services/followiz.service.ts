import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpParams, HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class FollowizService {
  constructor(private API: ApiService, private http: HttpClient) { }

  fetchServices(value): any {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.API.post('admin/services/services', {value: value}, headers);
  }
  

  createCategories(category): any {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.API.post('admin/services/savecategory', category, headers);
  }


  saveServices(service): any {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    console.log('service', service);
    return this.API.post('admin/services/saveservice', service, headers);
  }

  saveProvider(provider): any {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    console.log('provider', provider);
    return this.API.post('admin/services/saveprovider', provider, headers);
  }



  getProviders() {
    return this.API.get('admin/services/getprovider');

  }
  getCategories() {
    return this.API.get('admin/services/getcategory');
  }

  getServices() {
    return this.API.get('user/services/getservices');
  }


}
