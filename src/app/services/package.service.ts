import { ApiService} from './api.service'
import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PackageService {

  constructor(private API: ApiService) { }
  
  getPackages() {
    return this.API.get('admin/packages/getPackage');
  }

  getPlatform() {
    return this.API.get('admin/packages/getPlatform');
  }
  
createPlatform(platform): any {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.API.post('admin/packages/saveplatform', platform, headers);
  }
  

  savePackageService(newPackage){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    console.log('package details', newPackage)
    return this.API.post('admin/packages/savePackage', newPackage, headers);
  }
}
