import { ApiService } from './api.service';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient,
              private API: ApiService) { }

  private baseURL = environment.API_END_URL;
  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private getHeaders() {
    const token = localStorage.getItem('token');
    const headers = this.headers.append('Authorization', 'Bearer '+ token);
    return headers;
  }

getUsers() {
  return this.API.get('admin/users/getusers');
}


  addOrder(data) {
    return this.API.post('user/addOrder',data,this.getHeaders());
  }

  saveProfileChange(params){
    let token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Authorization':'Bearer '+ token
    });
    return this.API.post('user/auth/saveProfileChange',params,this.getHeaders());
  }

  saveSecurityChange(params){
    let token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Authorization':'Bearer '+ token
    });
    // console.log('token', token);
    return this.API.post('user/auth/updatePassword',params,this.getHeaders());
  }

  deleteUser(userId) {
    return this.API.post('admin/users/deleteUser',userId,this.getHeaders());
  }

  blockUser(userId) {
    return this.API.post('admin/users/blockUser',userId,this.getHeaders());
  }



  saveProfileImage(params){
    let token = localStorage.getItem('token');

    const headers = new HttpHeaders({
      'mimeType': 'multipart/form-data',
      'Authorization':'Bearer '+ token
      });
    return this.API.post('user/auth/saveProfileImage',params,headers);
  }

}
