import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  constructor() { }
  loadExternalScript(url) {
    return new Promise(resolve => {
      const scriptElement = document.createElement('script');
      scriptElement.src = url;
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    });
  }
  loadExternalStyle(url) {
    return new Promise(resolve => {
      const styleElement = document.createElement('link');
      styleElement.href = url;
      styleElement.onload = resolve;
      document.head.appendChild(styleElement);
    });
  }
}
