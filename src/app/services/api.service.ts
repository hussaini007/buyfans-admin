import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import {HttpParams, HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseURL = environment.API_END_URL;
  public headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  constructor(private http: HttpClient) { }

  get(path, headers = this.headers): Observable<any> {
    return this.http.get(`${this.baseURL}${path}`, { headers: headers });
  }
  // getParams(path, headers = this.headers): Observable<any> {
  //   let params = new HttpParams();
  //   params = params.append('city', 'berlin');
  //   return this.http.get(`${this.baseURL}${path}`,{params:params});
  // }

  post(path, data, headers = this.headers): Observable<any> {
    return this.http.post<any>(`${this.baseURL}${path}`, data, { headers: headers });
  }

  put(path) {
    return this.http.put(`${this.baseURL}${path}`, JSON.stringify({}));
  }

  delete(path) {
    return this.http.delete(`${this.baseURL}${path}`);
  }

  // getUsers(path, headers = this.headers): Observable<any>  {

  //   // let headers = new HttpHeaders({
  //   //   // 'Content-Type': 'application/x-www-form-urlencoded',
  //  //      'Authorization': "Bearer "+'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU2NzZkNGVmZDc3MTYzMzY5YTBlNTNlMDViODg1MzlhZmM5ODQ4MTZiNzY4YzBlZGE0MGI1OTM0ZGQzYjI2YzE5Yzk3MjkyZWM3YzBlOTc1In0.eyJhdWQiOiIxIiwianRpIjoiNTY3NmQ0ZWZkNzcxNjMzNjlhMGU1M2UwNWI4ODUzOWFmYzk4NDgxNmI3NjhjMGVkYTQwYjU5MzRkZDNiMjZjMTljOTcyOTJlYzdjMGU5NzUiLCJpYXQiOjE1Njk1ODc4MDAsIm5iZiI6MTU2OTU4NzgwMCwiZXhwIjoxNjAxMjEwMjAwLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.OLs-duJOIE1s7jJVP5YgcfyfvPUDswq4cUInYpd-DFEMTDe8xq1AamzH0O0hoJ3ZZbeDi8w5v1u2XoYiTtv-NL57ygPYpa5TBWzQYAxTONYo-iMFYr3cd8J7TCrKdIMhIbYlcztuHPBRVkxvbEhlzeDe6pWctO7ldDGPvDt1ZtebDRqOouQatg-youbcKZxB8Nw8pQ6rPT7M9-i1ozIio1BfRS5ORj4yebearcqlJS9ioY6CluUTIvIgXfFe0JRThaLoAVqj5bn60gdcfgHXnt6m_lXtwogMUTjpXZllQuz4HnW9n7rWYnzmcDLPRLXaFxiQqQ3tnfin5tVVexaQUHK3rFkcJ2H48g10oelz3e9PzmqlifyzeLykzlRz_bxFt2XpuUq0_hATHcTOGL35-JLU0TmVGoUJiuXPmcjH5ZkD36bCGh98LWr75YxYKn8eXWL0MWp7OpxsPsEaBp9r6cWoeI32HUWnEFkGskPIxXWqQlykUr1902ebeAO7CtdoQa9gyvpP2chgRHzoQi4u0oefpoffVBC29TcORapP2Q7xDbxPeoz0mI2tWONWQnzDtsdXbIrjoxZq_7cZ2G4LCN917TzavngDC-knzIu5EWrBxxKF93J_NtbJ9fzUngpwjuNkyhPHfduMSdDDNs_-12iwoDC4aiYNDxO4NPRx6b8'
  //  //    });
  //     // console.log(headers);
  //     const token = localStorage.getItem('token');
  //     console.log(token);
  //     // let httpHeaders = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

  //       headers = this.headers.append('Authorization', 'Bearer '+token);
        
  //   return this.http.get(`${this.baseURL}${path}`,{ headers: headers });
  // }
  // initializePayment(path, headers = this.headers) : Observable<any>{
      
  //     const token = localStorage.getItem('token');
  //     console.log(token);
  //     // let httpHeaders = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

  //       headers = this.headers.append('Authorization', 'Bearer '+token);
        
  //   return this.http.get(`${this.baseURL}${path}`,{ headers: headers });
  // }
  // checkout(path, data, headers = this.headers) : Observable<any>{
      
  //     const token = localStorage.getItem('token');
  //     console.log(token);
  //     // let httpHeaders = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

  //       headers = this.headers.append('Authorization', 'Bearer '+token);
        
  //   return this.http.post(`${this.baseURL}${path}`,data,{ headers: headers });
  // }
  // addOrder(path, data, headers = this.headers) : Observable<any>{
      
  //     const token = localStorage.getItem('token');
  //     console.log(token);
  //     // let httpHeaders = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

  //       headers = this.headers.append('Authorization', 'Bearer '+token);
        
  //   return this.http.post(`${this.baseURL}${path}`,data,{ headers: headers });
  // }
}