import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class ProfilePicService {
  private profilePictureSubject: BehaviorSubject<String>;
  public currentImage: Observable<String>;
  constructor(){
    this.profilePictureSubject = new BehaviorSubject<string>(JSON.parse(localStorage.getItem('user_image')));
    this.currentImage = this.profilePictureSubject.asObservable();
  }

  loadPicture(url){
      this.profilePictureSubject.next(url);
  }
}

