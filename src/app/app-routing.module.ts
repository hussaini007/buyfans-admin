import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TasksComponent } from './admin/tasks/tasks.component';
import { AdminPackagesComponent } from './admin/packages/packages.component';
import { ServicesComponent } from './admin/service/services.component';
import { AdminSettingsComponent } from './admin/settings/settings.component';
import { AdminDashboardComponent } from './admin/dashboard/dashboard.component';
import { TicketsComponent } from './admin/tickets/tickets.component';
import { ReportsComponent } from './admin/reports/reports.component';
import { AppearanceComponent } from './admin/appearance/appearance.component';
import { PaymentsComponent } from './admin/payments/payments.component';
import { CancelComponent } from './admin/cancel/cancel.component';
import { DripFeedComponent } from './admin/dripfeed/dripfeed.component';
import { SubscriptionsComponent } from './admin/subscriptions/subscriptions.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { UsersComponent } from './admin/users/users.component';
import { TicketsMessageComponent } from './admin/tickets/tickets-message/tickets-message.component';



const routes: Routes = [
  {path:'services',  component:ServicesComponent},
  {path:'dashboard',  component:AdminDashboardComponent},
  {path:'settings',  component:AdminSettingsComponent},
  {path:'users',  component:UsersComponent},
  {path:'orders',  component:OrdersComponent},
  {path:'subscription',  component:SubscriptionsComponent},
  {path:'dripfeed',  component:DripFeedComponent},
  {path:'tasks',  component:TasksComponent},
  {path:'payments',  component:PaymentsComponent},
  {path:'tickets',  component:TicketsComponent},
  {path:'reports',  component:ReportsComponent},
  {path:'appearance',  component:AppearanceComponent},
  {path:'packages',  component:AdminPackagesComponent},
  {path:'cancel',  component:CancelComponent},
  {path:'ticket-message',  component:TicketsMessageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
